#!/usr/bin/env python

import click
from sgdf_intranet_extractor.intranet import Intranet

@click.command()
@click.argument("extraction_type", default="adherent", required=True)
@click.option('-u', '--user','user', required=True)
@click.option('-f', '--filename', 'filename')
@click.password_option(confirmation_prompt=False)
def cli(user, password, filename, extraction_type):
    """Example script."""

    intranet = Intranet()
    intranet.login(user, password)

    if extraction_type in ["formation", "qualification", "diplome"]:
       extractions = intranet.extract_all(extraction_type)
       table = intranet.all_to_tablib(extractions)
       if filename == None or filename == "":
           intranet.write_tablib(table, extraction_type, "csv")
    elif extraction_type == "adherent":
       extraction = intranet.extract("adherent")
       table = intranet.to_tablib(extraction)
       if filename == None or filename == "":
           intranet.write_tablib(table, extraction_type, "csv")
    elif extraction_type == "structure":
        intranet.extract_structure()

if __name__ == '__main__':
    cli(obj={})
