from setuptools import setup

setup(name='sgdf-intranet-extractor',
      version='0.1',
      description='To Extract from SGDF intranet',
      url='http://github.com/ixeft/sgdf-intranet-extractor',
      author='François-Xavier Lyonnet du Moutier',
      author_email='fx.du.moutier@gmail.com',
      license='AGPL',
      packages=['sgdf_intranet_extractor'],
      install_requires=[
        'Click',
        'beautifulsoup4',
        'mechanize',
        'tablib',
        'lxml'
      ],
      entry_points='''
        [console_scripts]
        sgdf-extract=sgdf_intranet_extract:cli
    ''',
      zip_safe=False)
