import mechanize
from bs4 import BeautifulSoup, SoupStrainer
import http
import csv
import copy
import tqdm
import tablib
from .consts import code_formations, code_diplomes, code_qualifications
import logging
#import sys, logging
#logger = logging.getLogger("mechanize")
#logger.addHandler(logging.StreamHandler(sys.stdout))
#logger.setLevel(logging.DEBUG)



class Intranet:
    def __init__(self):
        cook = http.cookiejar.CookieJar()
        self.req = mechanize.Browser()
        self.req.set_cookiejar(cook)
        self.req.set_handle_robots(False)

        #self.req.set_debug_http(True)

        self.loggedin = False

    def login(self, username, password):
        self.req.open("https://intranet.sgdf.fr/Specialisation/Sgdf/Default.aspx")

        self.req.select_form(nr=0)
        self.req.form['login'] = username 
        self.req.form['password'] = password
        self.req.submit()

        if self.req.response().code != 200 or 'err' in self.req.response().geturl():
            raise Exception("Authentication failed")

        self.loggedin = True
    def extract_structure(self):

        response = None
        print(self.req.geturl())

        for link in self.req.links():
            attrs = dict(link.attrs)
            if hasattr(attrs, "id") and str(attrs["id"]) == "ctl00_ctl00__hlVoirFicheStructureLogo":
                response = self.req.click_link(link)
                break

        soup = BeautifulSoup(response.read(), features="lxml")
        htable = soup.find(id="ctl00_ctl00_MainContent_TabsContent_TabContainerResumeStructure__tabHierarche__gvEnfants")

        rows = []
        for row in htable.find_all('tr'):
            rows.append([val.text for val in row.find_all('td')])

        print(rows)

    def extract(self, extraction_type = "adherent", code = "", adh_type= "1"):
        if not self.loggedin:
            raise Exception("Not logged in")
        req = self.req
        req.open("https://intranet.sgdf.fr/Specialisation/Sgdf/Adherents/ExtraireAdherents.aspx")
        req.select_form(nr=0)
        req.form.set_all_readonly(False)
        req.form.new_control('text', '__EVENTTARGET',{'value':"ctl00$MainContent$_btnExporter"})
        req.form.new_control('text', '__EVENTARGUMENT',{'value':""})
        req.form.new_control('text', '__eo_obj_states',{'value':""})
        req.form.new_control('text', '__eo_sc',{'value':""})
        req.form.new_control('text', '__LASTFOCUS',{'value':""})

        try:
            req.form['_eo_js_modules'] = ""
            req.form['_eo_obj_inst'] = ""
            req.form.find_control('ctl00$_btnOui').disabled = True
        except:
            pass

        # Adultes seulement !
        req.form['ctl00$MainContent$_ddCategorieMembre'] = [adh_type]
        if extraction_type == "formation":
            req.form['ctl00$MainContent$_ddFormation'] = [str(code)]
        elif extraction_type == "qualification":
            req.form['ctl00$MainContent$_ddQualification'] = [str(code)]
        elif extraction_type == "diplome":
            req.form['ctl00$MainContent$_ddDiplome'] = [str(code)]
        else: # default - adherent
            req.form['ctl00$MainContent$_cbExtraireIndividu'] = ['on']
            req.form['ctl00$MainContent$_cbExtraireInscription'] = ['on']

        response = req.submit()
        return response.read()

    def extract_all(self, extraction_type = "adherent", with_mbr_assoc=True):
        
        adh_types= ["1", "2"] if with_mbr_assoc else ["1"]

        if extraction_type == "formation":
            codes = code_formations
        elif extraction_type == "qualification":
            codes = code_qualifications
        elif extraction_type == "diplome":
            codes = code_diplomes
        elif extraction_type == "adherent" and with_mbr_assoc:
            codes = {"adherent": "adherent"}
        else:
            return []


        extractions = []
        for adh_type in adh_types: 
            for code, label in codes.items():
                logging.info("extracting {}".format(label))
                extractions.append(self.extract(extraction_type=extraction_type, code=code, adh_type = adh_type))

        return extractions

    
    def clean_extract(self, extract):
        rows = []
        for row in extract:
            if row and row[0] != '' and row[1] != '':
                if len(row) > 21:
                    if row[18] != '':
                        row[18] = "+33" + row[18][1:]
                    if row[19] != '':
                        row[19] = "+33" + row[19][1:]
                    if row[20] != '':
                        row[20] = "+33" + row[20][1:]
                    
                    if row[19] == '':
                        if row[20] != '':
                            row[19] = row[20]
                        elif row[18] != '':
                            row[19] = row[20]

                rows.append(row)
        return rows

    def to_tablib(self, extract):
        soup = BeautifulSoup(extract, features="lxml", parse_only=SoupStrainer(['table', 'tr', 'td']))
        htable = soup.find('table')
        rows = []
        for row in htable.find_all('tr'):
            rows.append([val.text for val in row.find_all('td')])
        
        headers = rows[0]
        rows = self.clean_extract(rows[1:])

        extract = tablib.Dataset(headers=headers)
        for row in rows:
            extract.append(row)
        return extract

    def all_to_tablib(self, extractions):
        if len(extractions) > 0:
            table = self.to_tablib(extractions[0])
            for extraction in extractions[1:]:
                ex = self.to_tablib(extraction)
                table.extend(ex)
            return table
        return []

    def write_tablib(self, table, filename="extraction", format = "csv"):
        with open(filename + "." + format, 'w') as f_output:
            f_output.write(table.export(format))
